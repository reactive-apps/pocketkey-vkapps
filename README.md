<img src="./docs/banner.png" alt="banner">

# PocketKey for VK Apps
2FA codes generator for VK Apps

## Open App
For view this app you need to open this [link](https://vk.com/app7023199)

## Technologies
*   React
*   Redux / react-redux
*   Rematch
*   VKUI
*   VKUI Connect / VKUI Connect Promise

## Developers
This project developing by Reactive Apps Team:
*   [Stepan Novozhilov](https://vk.me/this.state.user)

## Links
*   [Reactive Apps in VK](https://vk.com/reactapps)
*   [App in VK](https://vk.com/app7023199)