import { init } from "@rematch/core";

import navigator from "./navigator";
import issuers from "./issuers";

const models = {
    navigator,
    issuers,
};

const store = init({
    models
});

export default store;