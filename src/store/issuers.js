import VKStorage from "vk-storage";
import { getData, getCode } from "../utils/otp";
import { generateId } from "../utils/generator";

function unique(arr) {
    let obj = {};
    arr.forEach((x) => {
        let str = generateId(x.label.issuer, x.label.account);
        obj[str] = x;
    });
    return Object.keys(obj).map((x) => obj[x]);
}

const issuers = {
    state: [],
    reducers: {
        loaded(state, payload) {
            return payload;
        },
        push(state, payload) {
            return unique([...state, payload]);
        }
    },
    effects: (dispatch) => ({
        load() {
            VKStorage.__getAllKeys()
                .then((keys = []) => {
                    const result = [];
                    keys.filter((x) => !x.startsWith("app_")).forEach((key) => {
                        const issuer = VKStorage.get(key);
                        result.push({ ...getData(issuer), url: issuer, code: getCode(getData(issuer)) });
                    });
                    dispatch.issuers.loaded(result);
                });
        },
        updateCodes(payload, state) {
            let updatedState = [...state.issuers];
            updatedState = updatedState.map((issuer) => ({ ...issuer, code: getCode(issuer) }));
            dispatch.issuers.load(updatedState);
        },
        addIssuer({url, successCallback, errorCallback}) {
            const data = getData(url);
            const id = generateId(data.label.issuer, data.label.account);
            if(data.type !== "totp") return errorCallback();
            VKStorage.set(id, url);
            dispatch.issuers.push({ ...data, url, code: getCode(data) });
            successCallback();
        },
        deleteIssuer(url, state) {
            const id = state.issuers.findIndex((x) => x.url === url);
            if (id >= 0) {
                const data = getData(url);
                VKStorage.set(generateId(data.label.issuer, data.label.account), "");
                dispatch.issuers.loaded(state.issuers.filter((x) => x.url !== url));
            }
        }
    })
};

export default issuers;