import React from "react";
import { renderReportForm } from "tryout-sdk/dist/sdk";
import { connect } from "react-redux";
import View from "@vkontakte/vkui/dist/components/View/View";

import InitPanel from "./panels/Init";
import HomePanel from "./panels/Home";

const App = ({ activePanel, popout, history, goBack }) => (
		<View
            activePanel={activePanel}
            popout={renderReportForm(popout)}
			history={history}
			onSwipeBack={goBack}
        >
			<InitPanel id="init"/>
			<HomePanel id="home"/>
		</View>
);

const mapState = (state) => ({
    activePanel: state.navigator.active,
	popout: state.navigator.popout,
	history: state.navigator.history
});

const mapDispatch = (dispatch) => ({
    goBack: dispatch.navigator.goBack
});

export default connect(mapState, mapDispatch)(App);
