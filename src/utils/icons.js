const resolveIcon = (issuer) => {
    switch (issuer.toLowerCase()) {
        case "github": return "https://cdn.worldvectorlogo.com/logos/github-1.svg";
        case "twitter": return "https://cdn.worldvectorlogo.com/logos/twitter-3.svg";
        case "google": return "https://cdn.worldvectorlogo.com/logos/google-icon.svg";
        case "gitlab.com": return "https://d15shllkswkct0.cloudfront.net/wp-content/blogs.dir/1/files/2018/06/12647515_1027632597296534_4325090141023636807_n.png";
        default: return "https://vk.com/images/vkapp_i.png";
    }
};

export default resolveIcon;