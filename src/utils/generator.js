export const generateId = (issuer, account) => {
    let result = Buffer.from(issuer + ":" + account).toString("base64");
    result = result.split("+").join("-");
    result = result.split("/").join("_");
    result = result.replace(/=+$/, '');
    return result;
};