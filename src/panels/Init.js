import React from "react";
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import Spinner from "@vkontakte/vkui/dist/components/Spinner/Spinner";

const Init = ({ id }) => (
    <Panel
        id={id}
        theme="white"
    >
        <Spinner/>
    </Panel>
);

export default Init;