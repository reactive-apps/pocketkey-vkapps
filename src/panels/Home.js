import React, { useState, Fragment } from "react";
import { showReportForm } from "tryout-sdk/dist/sdk";
import { connect } from "react-redux";
import VKConnect from "@vkontakte/vkui-connect";
import Panel from "@vkontakte/vkui/dist/components/Panel/Panel";
import PanelHeader from "@vkontakte/vkui/dist/components/PanelHeader/PanelHeader";
import HeaderButton from "@vkontakte/vkui/dist/components/HeaderButton/HeaderButton";
import Avatar from "@vkontakte/vkui/dist/components/Avatar/Avatar";
import Group from "@vkontakte/vkui/dist/components/Group/Group";
import List from "@vkontakte/vkui/dist/components/List/List";
import Cell from "@vkontakte/vkui/dist/components/Cell/Cell";
import Counter from "@vkontakte/vkui/dist/components/Counter/Counter";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import InfoRow from "@vkontakte/vkui/dist/components/InfoRow/InfoRow";
import Progress from "@vkontakte/vkui/dist/components/Progress/Progress";
import CellButton from "@vkontakte/vkui/dist/components/CellButton/CellButton";
import Alert from "@vkontakte/vkui/dist/components/Alert/Alert";

import resolveIcons from "../utils/icons";

import Icon24Qr from "@vkontakte/icons/dist/24/qr";
import Icon24Bug from '@vkontakte/icons/dist/24/bug';

/*
    Timer Management
 */
let timerStarted = false;
let globalTime = 30;
const startTimer = (setTimer) => {
    if (!timerStarted) {
        setInterval(() => {
            if(globalTime > 0) return setTimer(globalTime - 1);
            return setTimer(30);
        }, 1000);
        timerStarted = true;
    }
};

/*
    Query params
 */
const query_params = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&").reduce((a, x) => {
    const data = x.split("=");
    a[decodeURIComponent(data[0])] = decodeURIComponent(data[1]);
    return a;
}, {});

const Home = ({ id, issuers, setPopout, addIssuer, deleteIssuer, updateCodes }) => {
    VKConnect.subscribe(({ detail: { type, data } }) => {
        if (type === "VKWebAppOpenQRResult") {
            return addIssuer({
                url: data.qr_data,
                successCallback: () => {
                    return (
                        <Alert
                            actions={[
                                {
                                    title: "Закрыть",
                                    style: "cancel",
                                    autoclose: true
                                }
                            ]}
                            onClose={() => setPopout(null)}
                        >
                            Аккаунт успешно добавлен!
                        </Alert>
                    );
                },
                errorCallback: () => {
                    return setPopout(
                        <Alert
                            actions={[
                                {
                                    title: "Закрыть",
                                    style: "cancel",
                                    autoclose: true
                                }
                            ]}
                            onClose={() => setPopout(null)}
                        >
                            Простите, но данный вид генерации кодов не поддерживается
                        </Alert>
                    )
                }
            });
        }
    });

    const [ manage, setManage ] = useState(false);

    const [ timer, setTimer ] = useState(30);
    globalTime = timer;
    startTimer(setTimer);

    if(timer === 30) {
        updateCodes();
        setTimer(29);
    }

    return (
        <Panel id={id}>
            <PanelHeader
                left={
                    ["mobile_android", "mobile_iphone"].indexOf(query_params["vk_platform"]) !== -1 ?
                    <HeaderButton onClick={() => VKConnect.send("VKWebAppOpenQR", {})}>
                        <Icon24Qr/>
                    </HeaderButton> : null
                }
            >
                PocketKeys
            </PanelHeader>
            {
                issuers.length > 0
                ?
                    <Fragment>
                        <Div style={{ background: "var(--background_content)" }}>
                            <InfoRow title={"Мы обновим ваши коды через: " + timer}>
                                <Progress value={timer*100/30} />
                            </InfoRow>
                        </Div>
                        <Group title="Мои аккаунты">
                            <List>
                                {issuers.map((data, key) => {
                                    return (
                                        <Cell
                                            key={key}
                                            before={
                                                <Avatar
                                                    src={resolveIcons(data.label.issuer)}
                                                    style={{
                                                        background: "var(--background_content)"
                                                    }}
                                                />
                                            }
                                            children={data.label.account}
                                            description={"Сервис: " + data.label.issuer}
                                            indicator={<Counter children={data.code} type="primary"/>}
                                            removable={manage}
                                            onRemove={() => deleteIssuer(data.url)}
                                        />
                                    );
                                })}
                            </List>
                            <CellButton
                                align="center"
                                level={!manage ? "danger" : "primary"}
                                children={!manage ? "Удалить аккаунты" : "Завершить"}
                                onClick={() => setManage(!manage)}
                            />
                        </Group>
                    </Fragment>
                    : 
                    <Fragment>
                        <Div style={{
                            background: "var(--background_content)",
                            marginTop: 10,
                            display: "flex",
                            flexDirection: "column",
                            alignItems: "center"
                        }}>
                            <img src={require("../assets/vk_rabbit.png")} width="250" height="250" alt=""/>
                            <h3
                                style={{
                                    marginBottom: 10,
                                    color: "var(--text_primary)"
                                }}
                            >
                                Добро пожаловать в PocketKeys!
                            </h3>
                            <p
                                style={{
                                    marginTop: 0,
                                    textAlign: "center",
                                    color: "var(--text_secondary)"
                                }}
                            >
                                {
                                    ["mobile_android", "mobile_iphone"].indexOf(query_params["vk_platform"]) !== -1
                                        ? "Чтобы добавить новые аккаунты, воспользуйтесь сканером QR-кодов в левом верхнем углу"
                                        : "Чтобы добавить новые аккаунты, откройте сервис с телефона и воспользуйтесь сканером QR-кодов в левом верхнем углу"
                                }
                            </p>
                        </Div>
                    </Fragment>
            }
            <CellButton
                align="center"
                before={<Icon24Bug fill="var(--accent)"/>}
                style={{ background: "var(--background_content)" }}
                onClick={() => showReportForm()}
            >
                Сообщить о баге
            </CellButton>
        </Panel>
    );
};

const mapState = (state) => ({
    issuers: state.issuers
});

const mapDispatch = (dispatch) => ({
    setPopout: dispatch.navigator.setPopout,
    addIssuer: dispatch.issuers.addIssuer,
    deleteIssuer: dispatch.issuers.deleteIssuer,
    updateCodes: dispatch.issuers.updateCodes
});

export default connect(mapState, mapDispatch)(Home);