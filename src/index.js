import React from "react";
import ReactDOM from "react-dom";
import oldConnect from "@vkontakte/vkui-connect";
import connect from "@vkontakte/vkui-connect-promise";
import { init as toInit } from "tryout-sdk/dist/sdk";
import VKStorage from "vk-storage";
import "@vkontakte/vkui/dist/vkui.css";

import { Provider } from "react-redux";
import store from "./store";

import App from "./App";

// TryOut
toInit({
    product_id: "FBAN70ChKDGyzWVfdNbd",
    catcherEnabled: true
});

// Init VK App
connect.send("VKWebAppInit", {});
connect.send("VKWebAppGetAuthToken", {
    app_id: 7023199,
    scope: "friends"
})
    .then((response) => {
        VKStorage.init({ access_token: response.data.access_token, connect })
            .then(() => {
                store.dispatch.issuers.load();
                store.dispatch.navigator.goForce("home");
            })
            .catch(console.error);
    })
    .catch(console.error);

// Schema changer
oldConnect.subscribe(({ detail: { type, data } }) => {
    if(type === "VKWebAppUpdateConfig") {
        let schemeAttribute = document.createAttribute('scheme');
        schemeAttribute.value = data.scheme ? data.scheme : 'client_light';
        document.body.attributes.setNamedItem(schemeAttribute);
    }
});

ReactDOM.render(<Provider store={store} children={<App/>} />, document.getElementById("root"));
